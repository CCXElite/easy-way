import telebot
import database
import uuid
from telebot import types

bot = telebot.TeleBot('5155973605:AAHAZj-Onpgsh_TDGapDJWSF_ToOEgvBi0M')


class Message:
    def __init__(self, request_id, chat_id, section, fio, position, problem, photo):
        self.request_id = request_id
        self.chat_id = chat_id
        self.section = section
        self.fio = fio
        self.position = position
        self.problem = problem
        self.photo = photo

    def set_request_id(self, request_id):
        self.request_id = request_id

    def set_chat_id(self, chat_id):
        self.chat_id = chat_id

    def set_section(self, section):
        self.section = section

    def set_fio(self, fio):
        self.fio = fio

    def set_position(self, position):
        self.position = position

    def set_problem(self, problem):
        self.problem = problem

    def set_photo(self, photo):
        self.photo = photo


def show_buttons():
    buttons = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = types.KeyboardButton('РМК')
    button2 = types.KeyboardButton('РМД')
    button3 = types.KeyboardButton('Терминал')
    button4 = types.KeyboardButton('Эксплуатация')
    button5 = types.KeyboardButton('Расписание')
    button6 = types.KeyboardButton('Отчёты')
    buttons.add(button1)
    buttons.add(button2)
    buttons.add(button3)
    buttons.add(button4)
    buttons.add(button5)
    buttons.add(button6)
    return buttons


def send_info(request):
    send_to_id = 5087932489
    database.insert(request)
    send_str = 'Новое обращение!\nФИО: ' + request.fio + '\nРаздел: ' + request.section + '\nID заявки: ' \
               + str(request.request_id)
    bot.send_message(send_to_id, send_str)


def send_id(chat_id):
    bot.send_message(5087932489, chat_id)
    bot.send_message(504914125, chat_id)


# Функция, обрабатывающая команду /start
@bot.message_handler(commands=["start"])
def start(m, res=False):
    send_id(m.chat.id)
    bot.send_message(m.chat.id, 'Здравствуйте. Это техподдержка. Выберите раздел ПО, где возникла проблема.',
                     reply_markup=show_buttons())


# Получение сообщений от юзера
@bot.message_handler(content_types=['text'])
def handle_text(message):
    request_obj = Message('', '', '', '', '', '', '')
    request_obj.set_chat_id(message.chat.id)
    request_obj.set_request_id(uuid.uuid4())
    request_obj.set_section(message.text)
    bot.send_message(message.chat.id, 'Ваши фамилия и имя:', reply_markup=types.ReplyKeyboardRemove())
    bot.register_next_step_handler(message, get_fio, request_obj)


def get_fio(message, request_obj):
    request_obj.set_fio(message.text)
    bot.send_message(message.chat.id, 'Ваша должность:')
    bot.register_next_step_handler(message, get_position, request_obj)


def get_position(message, request_obj):
    request_obj.set_position(message.text)
    bot.send_message(message.chat.id, 'Опишите проблему. При необходимости прикрепите фотографии.')
    bot.register_next_step_handler(message, get_problem, request_obj)


def get_problem(message, request_obj):
    print(message)
    if message.photo:
        photo = bot.get_file_url(message.photo[-1].file_id)
        request_obj.set_photo(photo)
        if message.caption:
            problem = message.caption
            request_obj.set_problem(problem)
    elif message.document:
        bot.send_message(message.chat.id, 'Допустимо прикреплять только фотографии! Пожалуйста, прикрепите фотографию')
        bot.register_next_step_handler(message, get_problem, request_obj)
        return
    else:
        problem = message.text
        request_obj.set_problem(problem)

    send_info(request_obj)
    start_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    start_button = types.KeyboardButton('СТАРТ')
    start_markup.add(start_button)
    bot.send_message(message.chat.id, 'Обращение принято. В ближайшее время с вами свяжется специалист поддержки.',
                     reply_markup=start_markup)
    bot.register_next_step_handler(message, start)


# Запуск бота
bot.infinity_polling(timeout=10, long_polling_timeout=5)
